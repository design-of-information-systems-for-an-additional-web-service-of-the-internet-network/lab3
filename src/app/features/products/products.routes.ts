import { Routes } from '@angular/router';
// import { ProductContainer } from './containers/product';
import { ProductViewContainer } from './containers/product-view';
import { ProductsContainer } from './containers/products';

export const productsRoutes: Routes = [
  {
    path: '',
    component: ProductsContainer,
  },
  // {
  //   path: 'create',
  //   component: ProductContainer,
  // },
  // {
  //   path: ':id',
  //   component: ProductContainer,
  // },
  {
    path: ':id/view',
    component: ProductViewContainer,
  },
];
