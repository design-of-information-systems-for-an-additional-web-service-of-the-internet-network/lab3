import { AsyncPipe, NgIf } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, filter, map, switchMap } from 'rxjs';
import { ProductFormComponent } from '../../components/product-form';
import { Product } from '../../models';
import { BrandsService } from '../../services/brands';
import { CategoriesService } from '../../services/categories';
import { ProductsService } from '../../services/products';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    ProductFormComponent,
    AsyncPipe,
    NgIf,
  ],
  standalone: true,
  templateUrl: './product.container.html',
})
export class ProductContainer implements OnInit {
  private readonly activatedRoute = inject(ActivatedRoute);
  private readonly brandsService = inject(BrandsService);
  private readonly categoriesService = inject(CategoriesService);
  private readonly productsService = inject(ProductsService);
  private readonly router = inject(Router);

  public readonly brands$ = this.brandsService.brands$;
  public readonly categories$ = this.categoriesService.categories$;
  public readonly product$ = new BehaviorSubject<Product | null>(null);

  public ngOnInit(): void {
    this.categoriesService.loadCategories().subscribe();
    this.brandsService.loadBrands().subscribe();
    this.activatedRoute.params
      .pipe(
        filter((params) => params['id']),
        map((params) => params['id']),
        switchMap((id) => this.productsService.loadProduct(id)),
      )
      .subscribe((data) => {
        if (!(data instanceof Error)) {
          this.product$.next(data);
        }
      });
  }

  public createProduct(product: Product): void {
    this.productsService.createProduct(product).subscribe(() => {
      this.navigateToProducts();
    });
  }

  public updateProduct(product: Product): void {
    this.productsService.updateProduct(product).subscribe(() => {
      this.navigateToProducts();
    });
  }

  public navigateToProducts(): void {
    this.router.navigate(['/products']);
  }
}
