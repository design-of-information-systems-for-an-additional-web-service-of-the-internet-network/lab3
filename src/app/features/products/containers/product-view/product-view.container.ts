import { AsyncPipe, DatePipe, NgIf } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, map, switchMap } from 'rxjs';
import { ProductsTypeValuePipe } from '../../components/products-table/products-type-value.pipe';
import { Product } from '../../models';
import { BrandsService } from '../../services/brands';
import { CategoriesService } from '../../services/categories';
import { ProductsService } from '../../services/products';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    AsyncPipe,
    DatePipe,
    NgIf,
    ProductsTypeValuePipe,
  ],
  standalone: true,
  templateUrl: './product-view.container.html',
})
export class ProductViewContainer implements OnInit {
  private readonly activatedRoute = inject(ActivatedRoute);
  private readonly brandsService = inject(BrandsService);
  private readonly categoriesService = inject(CategoriesService);
  private readonly productsService = inject(ProductsService);
  private readonly router = inject(Router);

  public readonly brands$ = this.brandsService.brands$;
  public readonly categories$ = this.categoriesService.categories$;
  public readonly product$ = new BehaviorSubject<Product | null>(null);

  public ngOnInit(): void {
    this.categoriesService.loadCategories().subscribe();
    this.brandsService.loadBrands().subscribe();
    this.activatedRoute.params
      .pipe(
        map((params) => params['id']),
        switchMap((id) => this.productsService.loadProduct(id)),
      )
      .subscribe((data) => {
        if (!(data instanceof Error)) {
          this.product$.next(data);
        }
      });
  }

  public navigateToProducts(): void {
    this.router.navigate(['/products']);
  }
}
