import { AsyncPipe, NgIf } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductsFiltersComponent } from '../../components/products-filters';
import { ProductsTableComponent } from '../../components/products-table';
import { Product, ProductsFilter } from '../../models';
import { BrandsService } from '../../services/brands';
import { CategoriesService } from '../../services/categories';
import { ProductsService } from '../../services/products';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    AsyncPipe,
    NgIf,
    ProductsTableComponent,
    ProductsFiltersComponent,
  ],
  standalone: true,
  templateUrl: './products.container.html',
})
export class ProductsContainer implements OnInit {
  private readonly brandsService = inject(BrandsService);
  private readonly categoriesService = inject(CategoriesService);
  private readonly productsService = inject(ProductsService);
  private readonly router = inject(Router);

  public readonly brands$ = this.brandsService.brands$;
  public readonly categories$ = this.categoriesService.categories$;
  public readonly products$ = this.productsService.products$;

  public ngOnInit(): void {
    this.brandsService.loadBrands().subscribe();
    this.categoriesService.loadCategories().subscribe();
  }

  public createProduct(): void {
    this.router.navigate(['/products/create']);
  }

  public deleteProducts(): void {
    if (confirm('Do you want to delete all products?')) {
      this.productsService.deleteProducts().subscribe();
    }
  }

  public deleteProduct(product: Product): void {
    if (confirm(`Do you want to delete ${product.name} products?`)) {
      this.productsService.deleteProduct(product.id).subscribe();
    }
  }

  public openProduct(product: Product): void {
    this.router.navigate(['/products', product.id]);
  }

  public viewProduct(product: Product): void {
    this.router.navigate([`/products/${product.id}/view`]);
  }

  public changeFilter(filter: ProductsFilter): void {
    this.productsService.loadProducts(filter.categoryId).subscribe();
  }
}
