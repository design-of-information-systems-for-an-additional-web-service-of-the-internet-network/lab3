import { NgForOf, NgIf } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { FormsModule, NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Category, ProductsFilter } from '../../models';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    FormsModule,
    NgForOf,
    NgIf,
  ],
  selector: 'app-products-filters',
  standalone: true,
  templateUrl: './products-filters.component.html',
})
export class ProductsFiltersComponent implements OnInit, OnDestroy {
  @Input() categories!: Category[];

  @Output() readonly changeFilter = new EventEmitter<ProductsFilter>();

  @ViewChild('productsFilters', { static: true }) ngForm!: NgForm;

  public formValueChanges!: Subscription;
  public form: ProductsFilter = {
    categoryId: 'all',
  };

  public ngOnInit(): void {
    this.formValueChanges = this.ngForm.form.valueChanges.subscribe((values) => {
      this.changeFilter.emit(values);
    });
  }

  public ngOnDestroy(): void {
    this.formValueChanges.unsubscribe();
  }
}
