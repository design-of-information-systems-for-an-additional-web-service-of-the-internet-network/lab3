import { NgClass, NgForOf, NgIf } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, signal } from '@angular/core';
import { FormsModule, NgForm } from '@angular/forms';
import { Brand, Category, Product } from '../../models';
import { FileConverter } from '../../services/file-converter';
import { ProductBuilder } from '../../services/product-builder';
import { UniqueValidatorDirective } from '../../validators/unique';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    FormsModule,
    NgClass,
    NgIf,
    UniqueValidatorDirective,
    NgForOf,
  ],
  selector: 'app-product-form',
  standalone: true,
  templateUrl: './product-form.component.html',
})
export class ProductFormComponent implements OnChanges {
  @Input() brands!: Brand[];
  @Input() categories!: Category[];
  @Input() product!: Product | null;

  @Output() readonly createProduct = new EventEmitter<Product>();
  @Output() readonly updateProduct = new EventEmitter<Product>();

  public form!: Product;
  public imagePreview = signal<string | ArrayBuffer | null>(null);
  public loaded = signal(false);
  public visible = signal(false);

  public ngOnChanges(): void {
    this.form = new ProductBuilder(this.product!).build();
    this.imagePreview.set(this.product?.image ?? '');
    this.loaded.set(true);
  }

  public get minDate(): string {
    return new Date(new Date().setDate(new Date().getDate() + 1)).toJSON().split('T')[0];
  }

  public get maxDate(): string {
    const maxMonth = 3;
    const date = new Date().setMonth(new Date().getMonth() + maxMonth);

    return new Date(date).toJSON().split('T')[0];
  }

  public async submit(): Promise<void> {
    const product = new ProductBuilder(this.form!);

    if (this.product?.id) {
      this.updateProduct.emit(product
        .setImage(this.imagePreview())
        .setUpdatedAt()
        .build());
    } else {
      this.createProduct.emit(product
        .setId()
        .setImage(this.imagePreview())
        .setCreatedAt()
        .setUpdatedAt()
        .build());
    }

    this.visible.set(true);
  }

  public reset(form: NgForm): void {
    form.resetForm();
    this.visible.set(false);

    if (this.imagePreview()) {
      this.imagePreview.set(null);
    }
  }

  public async previewImage({ target }: Event): Promise<void> {
    const files = (target as HTMLInputElement).files;

    if (files?.length) {
      this.insertImage(await FileConverter.fileToBase64(files[0]) as string);
    }
  }

  public insertImage(image: string): void {
    this.form.image = image;
    this.imagePreview.set(image);
  }

  public deleteImage(): void {
    this.form.image = null;
    this.imagePreview.set(null);
  }
}
