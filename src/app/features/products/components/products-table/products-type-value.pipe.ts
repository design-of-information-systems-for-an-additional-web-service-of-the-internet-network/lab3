import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'typeValue',
  standalone: true,
})
export class ProductsTypeValuePipe implements PipeTransform {
  transform(data: { id: string; value: string; }[], id: string): string {
    return data.find((d) => d.id === id)?.value ?? '';
  }
}
