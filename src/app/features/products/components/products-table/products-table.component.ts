import { DatePipe, NgForOf, NgIf } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { Brand, Category, Product } from '../../models';
import { ProductsTypeValuePipe } from './products-type-value.pipe';

@Component({
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    DatePipe,
    NgForOf,
    NgIf,
    ProductsTypeValuePipe,
  ],
  selector: 'app-products-table',
  standalone: true,
  templateUrl: './products-table.component.html',
})
export class ProductsTableComponent {
  @Input() brands!: Brand[];
  @Input() categories!: Category[];
  @Input() products!: Product[];

  @Output() readonly deleteProduct = new EventEmitter<Product>();
  @Output() readonly openProduct = new EventEmitter<Product>();
  @Output() readonly viewProduct = new EventEmitter<Product>();
}
