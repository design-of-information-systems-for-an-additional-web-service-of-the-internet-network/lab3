export interface Product {
  brandId: string;
  categoryId: string;
  code: string;
  count: number | null;
  createdAt: string;
  description: string;
  expectedAt: string;
  guaranty: boolean;
  id: string;
  image: string | ArrayBuffer | null;
  name: string;
  price: number | null;
  status: 'active' | 'inactive' | 'expected' | null;
  trialPeriod: boolean;
  updatedAt: string;
}
