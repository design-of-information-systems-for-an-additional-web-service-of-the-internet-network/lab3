import { HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

export function httpError(error: HttpErrorResponse): Observable<Error> {
  alert(`HTTP ERROR: ${error.error.error.message}`);

  return throwError(() => new Error('Error'));
}
