import { Directive, inject, Input } from '@angular/core';
import { AbstractControl, AsyncValidator, NG_ASYNC_VALIDATORS, ValidationErrors } from '@angular/forms';
import { map, Observable, of } from 'rxjs';
import { ProductsService } from '../../services/products';

@Directive({
  providers: [{ provide: NG_ASYNC_VALIDATORS, useExisting: UniqueValidatorDirective, multi: true }],
  selector: '[appUnique]',
  standalone: true,
})
export class UniqueValidatorDirective implements AsyncValidator {
  @Input() appUnique!: 'name' | 'code';
  @Input() appUniqueId!: string;

  private readonly productsService = inject(ProductsService);

  validate(control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
    if (!control.value) {
      return of(null);
    }

    return this.productsService.loadProducts().pipe(
      map((products) => products.find((data) => {
        return data.id !== this.appUniqueId && data[this.appUnique]?.toLowerCase() === control.value.toLowerCase();
      })),
      map((product) => !!product),
      map((state) => state ? { unique: true } : null),
    );
  }
}
