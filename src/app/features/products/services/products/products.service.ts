import { HttpClient } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { BehaviorSubject, catchError, map, Observable, of, tap } from 'rxjs';
import { Product } from '../../models';
import { httpError } from '../../utils/http-error';

@Injectable({ providedIn: 'root' })
export class ProductsService {
  private readonly httpClient = inject(HttpClient);
  private readonly ngxIndexedDBService = inject(NgxIndexedDBService);
  private readonly table = 'products';

  public readonly products$ = new BehaviorSubject<Product[]>([]);

  public get products(): Product[] {
    return this.products$.getValue();
  }

  public loadProduct(id: string): Observable<Product | Error> {
    return this.httpClient.get<Product>(`https://khai.web3.stage.net.ua/api/products/${id}.json`).pipe(
      catchError(httpError),
    );
  }

  public loadProducts(categoryId?: string): Observable<Product[] | Error> {
    if (categoryId) {
      return this.httpClient.get<Product[]>(`https://khai.web3.stage.net.ua/api/products/${categoryId}.json`).pipe(
        tap((products) => this.products$.next(products)),
        catchError(httpError),
      );
    }

    return of([]);
  }

  public createProduct(product: Product): Observable<Product> {
    return this.ngxIndexedDBService.add(this.table, product);
  }

  public updateProduct(product: Product): Observable<Product> {
    return this.ngxIndexedDBService.update<Product>(this.table, product);
  }

  public deleteProduct(id: string): Observable<Product[]> {
    return this.ngxIndexedDBService.deleteByKey(this.table, id).pipe(
      map(() => this.products.filter((product) => product.id !== id)),
      tap((products) => this.products$.next(products)),
    );
  }

  public deleteProducts(): Observable<boolean> {
    return this.ngxIndexedDBService.clear(this.table).pipe(
      tap(() => this.products$.next([])),
    );
  }
}
