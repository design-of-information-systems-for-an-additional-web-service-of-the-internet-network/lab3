import { Product } from '../../models';

export class ProductBuilder {
  private readonly product: Product = {
    brandId: '',
    categoryId: '',
    code: '',
    count: null,
    createdAt: '',
    description: '',
    expectedAt: '',
    guaranty: false,
    id: '',
    image: null,
    name: '',
    price: null,
    status: null,
    trialPeriod: false,
    updatedAt: '',
  };

  constructor(data?: Product) {
    if (data) {
      this.product = { ...this.product, ...data };
    }
  }

  public setId(): this {
    this.product.id = window.crypto.randomUUID();

    return this;
  }

  public setCreatedAt(): this {
    this.product.createdAt = new Date().toISOString();

    return this;
  }

  public setUpdatedAt(): this {
    this.product.updatedAt = new Date().toISOString();

    return this;
  }

  public setImage(image: string | ArrayBuffer | null): this {
    this.product.image = image;

    return this;
  }

  public build(): Product {
    return this.product;
  }
}
