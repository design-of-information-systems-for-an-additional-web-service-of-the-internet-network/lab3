import { HttpClient } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { BehaviorSubject, catchError, Observable, tap } from 'rxjs';
import { Category } from '../../models';
import { httpError } from '../../utils/http-error';

@Injectable({ providedIn: 'root' })
export class CategoriesService {
  private readonly httpClient = inject(HttpClient);

  public readonly categories$ = new BehaviorSubject<Category[]>([]);

  public loadCategories(): Observable<Category[] | Error> {
    if (this.categories$.getValue().length) {
      return this.categories$;
    }

    return this.httpClient.get<Category[]>('https://khai.web3.stage.net.ua/api/categories.json').pipe(
      tap((response) => this.categories$.next(response)),
      catchError(httpError),
    );
  }
}
