import { HttpClient } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { BehaviorSubject, catchError, Observable, tap } from 'rxjs';
import { Brand } from '../../models';
import { httpError } from '../../utils/http-error';

@Injectable({ providedIn: 'root' })
export class BrandsService {
  private readonly httpClient = inject(HttpClient);

  public readonly brands$ = new BehaviorSubject<Brand[]>([]);

  public loadBrands(): Observable<Brand[] | Error> {
    if (this.brands$.getValue().length) {
      return this.brands$;
    }

    return this.httpClient.get<Brand[]>('https://khai.web3.stage.net.ua/api/brands.json').pipe(
      tap((response) => this.brands$.next(response)),
      catchError(httpError),
    );
  }
}
