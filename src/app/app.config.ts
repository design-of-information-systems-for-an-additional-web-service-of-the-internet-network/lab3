import { provideHttpClient } from '@angular/common/http';
import { ApplicationConfig, importProvidersFrom } from '@angular/core';
import { provideRouter } from '@angular/router';
import { DBConfig, NgxIndexedDBModule } from 'ngx-indexed-db';
import { routes } from './app.routes';

const dbConfig: DBConfig = {
  name: 'MyDb',
  version: 1,
  objectStoresMeta: [{
    store: 'products',
    storeConfig: { keyPath: 'id', autoIncrement: false },
    storeSchema: [
      { name: 'string', keypath: 'string', options: { unique: false } },
      { name: 'categoryId', keypath: 'categoryId', options: { unique: false } },
      { name: 'code', keypath: 'code', options: { unique: true } },
      { name: 'count', keypath: 'count', options: { unique: false } },
      { name: 'createdAt', keypath: 'createdAt', options: { unique: false } },
      { name: 'description', keypath: 'description', options: { unique: false } },
      { name: 'expectedAt', keypath: 'expectedAt', options: { unique: false } },
      { name: 'guaranty', keypath: 'guaranty', options: { unique: false } },
      { name: 'id', keypath: 'id', options: { unique: true } },
      { name: 'image', keypath: 'image', options: { unique: false } },
      { name: 'name', keypath: 'name', options: { unique: true } },
      { name: 'price', keypath: 'price', options: { unique: false } },
      { name: 'status', keypath: 'status', options: { unique: false } },
      { name: 'trialPeriod', keypath: 'trialPeriod', options: { unique: false } },
      { name: 'updatedAt', keypath: 'updatedAt', options: { unique: false } },
    ],
  }],
};

export const appConfig: ApplicationConfig = {
  providers: [
    importProvidersFrom(NgxIndexedDBModule.forRoot(dbConfig)),
    provideRouter(routes),
    provideHttpClient(),
  ],
};
